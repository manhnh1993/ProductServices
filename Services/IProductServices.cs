﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SchoppingCart_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
   

        [ServiceContract]
        public interface IProductServices
        {
            [OperationContract]
            [WebInvoke(Method = "GET",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               UriTemplate = "/GetItemDetails/")]
            List<ShoppingCartDataContract.ProductDataContract> GetItemDetails();
            // TODO: Add your service operations here

       

            // to Insert the Item Master
            [OperationContract]
            [WebInvoke(Method = "POST",
               RequestFormat = WebMessageFormat.Json,
               ResponseFormat = WebMessageFormat.Json,
               UriTemplate = "/addItemMaster")]
            bool addItemMaster(ShoppingCartDataContract.ProductDataContract itemDetails);


        }

        // Use a data contract as illustrated in the sample below to add composite types to service operations.
        public class ShoppingCartDataContract
        {


            [DataContract]
            public class ProductDataContract
            {
                [DataMember]
                public int Id { get; set; }

                [DataMember]
                public string Name { get; set; }

                [DataMember]
                public int CategoryId { get; set; }

                [DataMember]
                public decimal Price { get; set; }

                [DataMember]
                public int Stock { get; set; }

                //[DataMember]
                //public string Image { get; set; }
            }

            [DataContract]
            public class CategoryDataContract
            {
                [DataMember]
                public int Id { get; set; }

                [DataMember]
                public string Description { get; set; }
            }

        }
    }

