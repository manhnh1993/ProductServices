﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Business;
using Entities;

namespace SchoppingCart_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ProduceServices : IProductServices
    {
        private IProductManager _productManager;
        private ICategoryManager _categoryManager;
        public ProduceServices()
        {
            //_productManager = productManager;
            //_categoryManager = categoryManager;
            _productManager = new ProductManager();
            _categoryManager = new CategoryManager();
        }
        // This method is get the Toys details from Db and bind to list  using the Linq query
        public List<ShoppingCartDataContract.ProductDataContract> GetItemDetails()
        {
            return _productManager.FindAll().Select(x => new ShoppingCartDataContract.ProductDataContract()
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price,
                Stock = x.Stock,
                //Image = x.Image,
                CategoryId = x.CategoryId

            }).ToList();
        }

        public bool addItemMaster(ShoppingCartDataContract.ProductDataContract productDetail)
        {
            try
            {
                _productManager.Save(new Product()
                {
                    Id = productDetail.Id,
                    Name = productDetail.Name,
                    Price = productDetail.Price,
                    Stock = productDetail.Stock,
                    //Image = productDetail.Image,
                    CategoryId = productDetail.CategoryId
                });
            }
            catch (Exception ex)
            {
                throw new FaultException<string>
                     (ex.Message);
            }
            return true;
        }


    }
}

