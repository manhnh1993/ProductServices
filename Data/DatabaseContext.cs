﻿using System.Linq.Expressions;

namespace Data
{
    using System.Data.Entity;
    using Entities;
    
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Product> ProductList { get; set; }
        public DbSet<Category> CategoryList { get; set; }
    }
}
