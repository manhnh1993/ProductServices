﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public interface IProductManager
    {
        IQueryable<Product> FindAll();

        Product Find(int id);

        void Save(Product product);

        void Modify(Product product);

        void Delete(int id);

        IEnumerable<Category> CategoryList { get; }
    }
}
