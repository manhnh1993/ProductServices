﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public interface ICategoryManager
    {
        IQueryable<Category> FindAll();

        Category Find(int id);

        void Save(Category category);

        void Modify(Category category);

        void Delete(int id);
    }
}
